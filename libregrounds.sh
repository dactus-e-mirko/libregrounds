#!/bin/bash

# Define colors:
RED='\033[0;31m'
NC='\033[0m' # no color

findXorg() {
    if test -f $HOME/.xinitrc || test -f $HOME/.Xresources; then
        echo "Xorg found!"
        echo "Starting Libregrounds..."
    else
        echo -e "${RED}Error${NC}: Xorg not found!"
        echo "Please make sure you have installed it and
that it is running before executing this program."
    fi
}
findXorg
